//
// Copyright (C) 2023 Nathan Sharp.
//
// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at https://mozilla.org/MPL/2.0/
//
// This Source Code Form is "Incompatible With Secondary Licenses", as defined
// by the Mozilla Public License, v. 2.0.
//

use core::hash::BuildHasherDefault;
use std::collections::hash_map::RandomState;
use std::collections::{HashMap, HashSet};

use crate::{Passthru, Prehashed};

/// A secure choice of hasher for pre-hashing values.
#[cfg_attr(feature = "doc_cfg", doc(cfg(feature = "std")))]
pub type DefaultPrehasher = RandomState;

/// A standard hash set with [`Prehashed`] elements and [`Passthru`] hashing.
///
/// # Security
/// If the hash function used for prehashing is weak, the set may be vulnerable
/// to Denial of Service attacks.
#[cfg_attr(feature = "doc_cfg", doc(cfg(feature = "std")))]
pub type PrehashedSet<T> = HashSet<Prehashed<T, u64>, BuildHasherDefault<Passthru>>;

/// Constructs a new standard hash set with [`Prehashed`] elements and
/// [`Passthru`] hashing.
///
/// # Security
/// If the hash function used for prehashing is weak, the set may be vulnerable
/// to Denial of Service attacks.
#[cfg_attr(feature = "doc_cfg", doc(cfg(feature = "std")))]
#[must_use]
pub fn new_prehashed_set<T>() -> PrehashedSet<T> {
    PrehashedSet::with_hasher(Default::default())
}

/// Constructs a new standard hash set with [`Prehashed`] elements, [`Passthru`]
/// hashing, and the specified `capacity`.
///
/// # Security
/// If the hash function used for prehashing is weak, the set may be vulnerable
/// to Denial of Service attacks.
#[cfg_attr(feature = "doc_cfg", doc(cfg(feature = "std")))]
#[must_use]
pub fn new_prehashed_set_with_capacity<T>(capacity: usize) -> PrehashedSet<T> {
    PrehashedSet::with_capacity_and_hasher(capacity, Default::default())
}

/// A standard hash map with [`Prehashed`] keys and [`Passthru`] hashing.
///
/// # Security
/// If the hash function used for prehashing is weak, the map may be vulnerable
/// to Denial of Service attacks.
#[cfg_attr(feature = "doc_cfg", doc(cfg(feature = "std")))]
pub type PrehashedMap<K, V> = HashMap<Prehashed<K, u64>, V, BuildHasherDefault<Passthru>>;

/// Constructs a new standard hash map with [`Prehashed`] keys and [`Passthru`]
/// hashing.
///
/// # Security
/// If the hash function used for prehashing is weak, the set may be vulnerable
/// to Denial of Service attacks.
#[cfg_attr(feature = "doc_cfg", doc(cfg(feature = "std")))]
#[must_use]
pub fn new_prehashed_map<K, V>() -> PrehashedMap<K, V> {
    PrehashedMap::with_hasher(Default::default())
}

/// Constructs a new standard hash map with [`Prehashed`] keys, [`Passthru`]
/// hashing, and the specified `capacity`.
///
/// # Security
/// If the hash function used for prehashing is weak, the set may be vulnerable
/// to Denial of Service attacks.
#[cfg_attr(feature = "doc_cfg", doc(cfg(feature = "std")))]
#[must_use]
pub fn new_prehashed_map_with_capacity<K, V>(capacity: usize) -> PrehashedMap<K, V> {
    PrehashedMap::with_capacity_and_hasher(capacity, Default::default())
}
