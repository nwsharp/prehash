# prehash
The `prehash` crate provides the type `Prehashed`, which stores a value of any type along with a
precomputed hash. This makes it possible to avoid computing large expensive hashes many times, for
example when searching for a particular value in a variety of hash tables.

The crate also defines an extremely simple [`Hasher`], `Passthru`, which is tailor built for use
with `Prehashed`, as well as some convenience functions.

# Documentation
Inline rustdoc documentation is available. A mirror of this documentation is available at
<https://docs.rs/prehash>.

# Contributing
`prehash` is developed at [GitLab].

Reasonable performance, correctness, documentation, and ease-of-use contributions are always
welcome. [Bug reports][Issues] and feature suggestions are accepted through GitLab.

## Pull Requests
Please ensure pull requests match the existing coding style and are formatted with rustfmt.

Spelling and grammatical errors are considered bugs, so please use spell-checking facilities prior
to submitting a contribution.

## Contribution Agreement
By contributing, you grant all contributors a perpetual, worldwide, non-exclusive, no-charge,
royalty-free, irrevocable copyright license to reproduce, prepare derivative works of, publicly
display, publicly perform, relicense, sublicense, and distribute your contributions.

Additionally, you affirm that you are legally entitled to grant such license and that your
contributions are not and will not become patent-encumbered. In the event that you discover that
such affirmation was made in error, you agree to post notice of such error in a conspicuous place
(such as a [GitLab Issue][Issues]) within three days.

# License
`prehash` is licensed under the terms of the [Mozilla Public License, v. 2.0][MPL]. All Source Code
Forms are "Incompatible With Secondary Licenses", as described in *§3.3* of the license.

The corresponding SPDX license identifier is [`MPL-2.0-no-copyleft-exception`][SPDX].

# Copyright
This document is Copyright (C) 2020 Nathan Sharp.

Permission is granted to reproduce this document, in any form, free of charge. The source code form
of this document is subject to the terms of the Mozilla Public License, v. 2.0.

[GitLab]: https://gitlab.com/nwsharp/prehash
[`Hasher`]: https://doc.rust-lang.org/std/hash/trait.Hasher.html
[Issues]: https://gitlab.com/nwsharp/prehash/-/issues
[MPL]: https://mozilla.org/MPL/2.0
[SPDX]: https://spdx.org/licenses/MPL-2.0-no-copyleft-exception.html
